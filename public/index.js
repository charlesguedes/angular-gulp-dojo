(function (angular) {
  var app =  angular.module('angular-gulp-dojo', ['ngRoute']);

  app.config(function ($routeProvider) {
    $routeProvider.when('/', { templateUrl: 'modules/home/home.html' });
    $routeProvider.when('/Todo', { templateUrl: 'modules/Todo/Todo.html' });
  });

})(angular);
