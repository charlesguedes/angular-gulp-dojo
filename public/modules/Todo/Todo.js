(function(angular){
  'use strict';
  var app = angular.module('angular-gulp-dojo');
  app.controller('TodoCtrl', function($scope, $http){
    $scope.name = 'Charles';
    var url = "https://angular-gulp-dojo-backend.herokuapp.com/api/task/"

    $scope.$watch('name', function(newValue, oldValue) {
      console.log(newValue);
    })
    
    $http.get(url)
    .success(function(tasks){
      $scope.tasks=tasks;
    });
  });
})(angular);
